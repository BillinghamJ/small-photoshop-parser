﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using JEB.PhotoshopParser.Model;

namespace JEB.PhotoshopParser
{
  class Program
  {
    static void Main(string[] args)
    {
      var length = Marshal.SizeOf(typeof(PhotoshopDocument));
      var buffer = new byte[length];
      File.OpenRead("test.psd").Read(buffer, 0, length);

      var header = BytesToStruct<PhotoshopDocument>(buffer);
    }

    private static T BytesToStruct<T>(byte[] rawData) where T : struct
    {
      T result;
      RespectEndianness(typeof(T), rawData);
      
      var handle = GCHandle.Alloc(rawData, GCHandleType.Pinned);

      try
      {
        var rawDataPtr = handle.AddrOfPinnedObject();
        result = (T)Marshal.PtrToStructure(rawDataPtr, typeof(T));
      }
      finally
      {
        handle.Free();
      }

      return result;
    }

    private static byte[] StructToBytes<T>(T data) where T : struct
    {
      var rawData = new byte[Marshal.SizeOf(data)];
      var handle = GCHandle.Alloc(rawData, GCHandleType.Pinned);

      try
      {
        var rawDataPtr = handle.AddrOfPinnedObject();
        Marshal.StructureToPtr(data, rawDataPtr, false);
      }
      finally
      {
        handle.Free();
      }

      RespectEndianness(typeof(T), rawData);

      return rawData;
    }

    private static void RespectEndianness(Type type, byte[] data)
    {
      var fields = GetFieldsRecursive(type, f => f.IsDefined(typeof(EndianAttribute), false))
          .Select(f => new
          {
            Field = f,
            Attribute = (EndianAttribute)f.GetCustomAttributes(typeof(EndianAttribute), false)[0],
            Offset = Marshal.OffsetOf(f.DeclaringType, f.Name).ToInt32()
          }).ToList();

      foreach (var field in fields)
      {
        if ((field.Attribute.Endianness == Endianness.BigEndian && BitConverter.IsLittleEndian) ||
            (field.Attribute.Endianness == Endianness.LittleEndian && !BitConverter.IsLittleEndian))
        {
          Array.Reverse(data, field.Offset, Marshal.SizeOf(field.Field.FieldType));
        }
      }
    }

    private static IEnumerable<FieldInfo> GetFieldsRecursive(Type type, Func<FieldInfo, bool> predicate)
    {
      foreach (var field in type.GetFields().Where(f => f.IsDefined(typeof(StructAttribute), false) || predicate.Invoke(f)))
      {
        foreach (var subfield in GetFieldsRecursive(field.FieldType, predicate))
          yield return subfield;

        if (predicate.Invoke(field))
          yield return field;
      }
    }
  }
}
