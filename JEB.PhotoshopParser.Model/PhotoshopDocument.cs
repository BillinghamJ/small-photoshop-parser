﻿using System;
using System.Runtime.InteropServices;
using System.Text;

// ReSharper disable InconsistentNaming
namespace JEB.PhotoshopParser.Model
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct PhotoshopDocument
  {
    [Struct]
    [MarshalAs(UnmanagedType.Struct)]
    public FileHeader fileHeader;

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FileHeader
    {
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
      public byte[] _signature;

      [Endian(Endianness.BigEndian)]
      public ushort version;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
      public byte[] reserved;

      [Endian(Endianness.BigEndian)]
      public ushort channels;

      [Endian(Endianness.BigEndian)]
      public uint height;

      [Endian(Endianness.BigEndian)]
      public uint width;

      [Endian(Endianness.BigEndian)]
      public ushort depth;

      [Endian(Endianness.BigEndian)]
      public ushort colorMode;

      public string signature
      {
        get { return Encoding.ASCII.GetString(_signature); }
        set { _signature = Encoding.ASCII.GetBytes(value); }
      }

      public enum Depth : ushort
      {
        D1 = 1,
        D8 = 8,
        D16 = 16,
        D32 = 32
      }

      public enum ColorMode : ushort
      {
        Bitmap = 0,
        Grayscale = 1,
        Indexed = 2,
        RGB = 3,
        CMYK = 4,
        Multichannel = 7,
        Duotone = 8,
        Lab = 9
      }
    }
  }

  [AttributeUsage(AttributeTargets.Field)]
  public class StructAttribute : Attribute
  {
  }

  [AttributeUsage(AttributeTargets.Field)]
  public class EndianAttribute : Attribute
  {
    public Endianness Endianness { get; private set; }

    public EndianAttribute(Endianness endianness)
    {
      Endianness = endianness;
    }
  }

  public enum Endianness
  {
    BigEndian,
    LittleEndian
  }
}
// ReSharper restore InconsistentNaming
